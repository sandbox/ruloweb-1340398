<?php
// $Id$

/**
 * @file
 * Provide special views data and handlers for draggableviews_menu.module
 */

/**
 * Implementation of hook_views_data()
 */
function draggableviews_menu_views_data() {
  $data['draggableviews_menu_menu_node_parent']['table']['group'] = t('Menu');

  $data['draggableviews_menu_menu_node_parent']['table']['join']['node'] = array(
    'table' => 'menu_node',
    'left_table' => 'draggableviews_menu_menu_links',
    'left_field' => 'plid',
    'field' => 'mlid',
  );

  $data['draggableviews_menu_menu_links']['table']['join']['node'] = array(
    'table' => 'menu_links',
    'left_table' => 'draggableviews_menu_menu_node',
    'left_field' => 'mlid',
    'field' => 'mlid',
  );

  $data['draggableviews_menu_menu_node']['table']['join']['node'] = array(
    'table' => 'menu_node',
    'left_field' => 'nid',
    'field' => 'nid',
  );

  $data['draggableviews_menu_menu_node_parent']['nid'] = array(
    'title' => t("Node parent's nid"),
    'help' => t("Returns the nid of the node's parent."),
    'field' => array(
      'click sortable' => TRUE,
    ),
  );

  return $data;
}