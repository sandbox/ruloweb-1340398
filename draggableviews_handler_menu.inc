<?php
// $Id$

/**
 * @file
 * The menu handler.
 */

/*
 * Menu handler.
 */
class draggableviews_handler_menu extends draggableviews_handler {
  function init($field_name, &$view) {
    parent::init($field_name, $view);
  }

  function save($nid, $value) {
    switch ($this->type) {
      case 'order':
        $mlid = db_result(db_query("SELECT mlid FROM {menu_node} WHERE nid = %d", $nid));
        $menu_link = menu_link_load($mlid);
        $menu_link['weight'] = $value;
        menu_link_save($menu_link);
        break;

      case 'hierarchy':
        $mlid = db_result(db_query("SELECT mlid FROM {menu_node} WHERE nid = %d", $nid));
        $plid = db_result(db_query("SELECT mlid FROM {menu_node} WHERE nid = %d", $value));
        $menu_link = menu_link_load($mlid);
        $menu_link['plid'] = $plid;
        menu_link_save($menu_link);
        break;
    }
  }

  function get_form_element($value, $attributes = array()) {
    switch ($this->type) {
      default:
        $options = array();
        for ($i = $this->range_start; $i <= $this->range_end; $i++) $options[$i] = $i;

        return array(
          '#type'  => 'select',
          '#name'  => $attributes['field_name'],
          '#value' => $value,
          '#options' => $options,
          '#attributes' => array('class' => $attributes['class']),
        );
        break;

      case 'hierarchy':
        return array(
          '#type' => 'hidden',
          '#name' => $attributes['field_name'],
          '#value' => $value,
          '#attributes' => array('class' => $attributes['class']),
        );
        break;
    }
  }
}