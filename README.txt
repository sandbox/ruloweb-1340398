USAGE:

* Create a node view.
* Add the "Menu: Weight" and "Menu: Menu: Node parent's nid" fields.
* Add the "Menu: Weight" sort criterial.
* In "Basic settings", select the "Draggable Table" style.
* In "Draggable Table" configurations select the "Weight" field for "Order Field" and the "Node parent's id" for "Parent" field. In both select "Menu" as handler.
* Update "Draggable Table" configurations
* Create a page display.
* Save the view and that's it! enjoy it!
